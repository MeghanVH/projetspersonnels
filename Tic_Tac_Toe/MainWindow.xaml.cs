﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tic_Tac_Toe
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Initialisation des joueurs -> pair = X - impair = O
        public int joueur = 2;
        //Compteur de tours
        public int tours = 0;
        //Compteur de score
        public int score1 = 0;
        public int score2 = 0;
        public int scoreEgalite = 0;

        private void ClickBouton(object sender, RoutedEventArgs e)
        {
            Button bouton = (Button)sender;
            //Empêche de recliquer sur le même bouton
            if (bouton.Content == "")
            {
                //Fait que le bouton prend un X ou un O
                if (joueur % 2 == 0)
                {
                    bouton.Content = "X";
                    //bouton.Background = Brushes.Blue;
                    joueur++;
                    tours++;
                }
                else
                {
                    bouton.Content = "O";
                    //bouton.Background = Brushes.Red;
                    joueur++;
                    tours++;
                }
                //Désactive le bouton sélectionné
                //bouton.IsEnabled = false;

                #region Montrer Tour
                MontrerTour(); 
                #endregion

                if (SiEgalite() == true)
                {
                    MessageBox.Show("Égalité !");
                    int scoreEgalite = Convert.ToInt32(ScoreEgalite.Content);
                    ScoreEgalite.Content = "" + (scoreEgalite + 1);
                    NouvelleGrille();
                }
                if (Gagnant() == true)
                {
                    if (bouton.Content == "X")
                    {
                        MessageBox.Show("X a gagné");
                        int score2 = Convert.ToInt32(ScoreX.Content);
                        ScoreX.Content = "" + (score2 + 1);
                        NouvelleGrille();
                    }
                    else
                    {
                        MessageBox.Show("O a gagné !");
                        int score2 = Convert.ToInt32(ScoreO.Content);
                        ScoreO.Content = "" + (score2 + 1);
                        NouvelleGrille();

                    }
                }
            }
        }

        //le bouton nouveau jeu génère une nouvelle grille 
        private void GenererGrille(object sender, RoutedEventArgs e)
        {
            NouvelleGrille();
        }
        #region Fonction MontrerTour
        private void MontrerTour()
        {
            if (tours % 2 == 0)
            {
                TourJoueur.Content = "Joueur 1";
            }
            else
            {
                TourJoueur.Content = "Joueur 2";
            }
        }
        #endregion


        #region Fonction qui crée une nouvelle grille
        private void NouvelleGrille()
        {
            joueur = 2;
            tours = 0;
            TourJoueur.Content = "Joueur 1";
            //Fait que tous les boutons redeviennent vides
            Bouton1.Content = Bouton2.Content = Bouton3.Content = Bouton4.Content = Bouton5.Content = Bouton6.Content = Bouton7.Content = Bouton8.Content = Bouton9.Content = "";
        }
        #endregion

        #region Fonction qui vérifie l'égalité
        private bool SiEgalite()
        {
            //Vérifie que personne n'a gagné avant de déclarer l'égalité
            if ((tours == 9)&&(Gagnant()==false))
                return true;
            else
                return false;
        }
        #endregion

        #region Fonction qui vérifie la victoire
        private bool Gagnant()
        {
            //Horizontal
            if ((Bouton1.Content == Bouton2.Content) && (Bouton2.Content == Bouton3.Content) && Bouton1.Content != "")
                return true;
            else if ((Bouton4.Content == Bouton5.Content) && (Bouton5.Content == Bouton6.Content) && Bouton4.Content != "")
                return true;
            else if ((Bouton7.Content == Bouton8.Content) && (Bouton8.Content == Bouton9.Content) && Bouton7.Content != "")
                return true;
            //Vertical
            if ((Bouton1.Content == Bouton4.Content) && (Bouton4.Content == Bouton7.Content) && Bouton1.Content != "")
                return true;
            else if ((Bouton2.Content == Bouton5.Content) && (Bouton5.Content == Bouton8.Content) && Bouton2.Content != "")
                return true;
            else if ((Bouton3.Content == Bouton6.Content) && (Bouton6.Content == Bouton9.Content) && Bouton3.Content != "")
                return true;
            //Diagonal
            if ((Bouton1.Content == Bouton5.Content) && (Bouton5.Content == Bouton9.Content) && Bouton1.Content != "")
                return true;
            else if ((Bouton3.Content == Bouton5.Content) && (Bouton5.Content == Bouton7.Content) && Bouton3.Content != "")
                return true;
            else
                return false;
        }
        #endregion

        private void ClickEffacerScore(object sender, RoutedEventArgs e)
        {
            ScoreEgalite.Content = "0";
            ScoreX.Content = "0";
            ScoreO.Content = "0";
            NouvelleGrille();
        }
    }
}
