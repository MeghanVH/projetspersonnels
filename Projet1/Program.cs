﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region RécupérationNoms
            Console.WriteLine("Bienvenue au jeu du morpion !");
            Console.WriteLine("Les règles sont simples: il suffit d'aligner 3 fois O ou X dans un tableau.");
            Console.WriteLine("Quel est le nom du premier joueur ?");
            string joueurUn = Console.ReadLine();
            Console.WriteLine("Quel est le nom du second joueur ?");
            string joueurDeux = Console.ReadLine();
            Console.WriteLine($"{joueurUn} est O et {joueurDeux} est X");
            Console.ReadKey();
            Console.Clear();
            #endregion

            #region CréationVariables
            int choix = 0;
            bool jeu = true;
            #endregion

            while (jeu == true)
            {
                bool jeuGagne = false;
                int tour = 1;
                while (jeuGagne == false)
                {
                    DessinPlateau();
                    if (tour == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"C'est le tour de {joueurUn}");
                    }
                    else if (tour == 2)
                    {
                        Console.WriteLine($"C'est le tour de {joueurDeux}");
                    }
                    bool input = false;
                    while (input == false)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Choisir un nombre entre 1 et 9 pour prendre une position :");
                        choix = int.Parse(Console.ReadLine());
                        if (choix > 0 && choix < 10)
                        {
                            input = true;
                        }
                    }
                    #region Positionnement
                    if (tour == 1)
                    {
                        if (position[choix] == "X")
                        {
                            Console.WriteLine("Position occupée, choisis une nouvelle position :");
                            Console.Clear();
                        }
                        else
                            position[choix] = "O";
                    }
                    if (tour == 2)
                    {
                        if (position[choix] == "O")
                        {
                            Console.WriteLine("Position occupée, choisis une nouvelle position :");
                            Console.ReadLine();
                            Console.Clear();
                        }
                        else
                            position[choix] = "X";  
                        #endregion
                    }
                    jeuGagne = Reussite();
                    #region RéinitialisationTours
                    if (jeuGagne == false)
                    {
                        if (tour == 1)
                        {
                            tour = 2;
                        }
                        else if (tour == 2)
                        {
                            tour = 1;
                        } 
                        #endregion
                        Console.Clear();
                    }
                    #region FinJeu
                    if (jeuGagne == true)
                    {
                        if (tour == 1)
                        {
                            Console.WriteLine($"{joueurUn} a gagné !");
                        }
                        else if (tour == 2)
                        {
                            Console.WriteLine($"{joueurDeux} a gagné !");
                        }
                    }
                    else
                    {
                        if (position[1] != "1" && position[2] != "2" && position[3] != "3" && position[4] != "4" && position[5] != "5" && position[6] != "6" && position[7] != "7" && position[8] != "8" && position[9] != "9")
                        {
                            Console.WriteLine("Personne n'a gagné !");
                            jeuGagne = true;
                        }
                    } 
                    #endregion
                }
                Console.ReadKey();
            }

        }

        #region JeuGagné
        public static bool Reussite()
        {
            if (position[1] == "O" && position[2] == "O" && position[3] == "O")
                return true;
            else if (position[4] == "O" && position[5] == "O" && position[6] == "O")
                return true;
            else if (position[7] == "O" && position[8] == "O" && position[9] == "O")
                return true;
            else if (position[1] == "O" && position[5] == "O" && position[9] == "O")
                return true;
            else if (position[7] == "O" && position[5] == "O" && position[3] == "O")
                return true;
            else if (position[1] == "O" && position[4] == "O" && position[7] == "O")
                return true;
            else if (position[2] == "O" && position[5] == "O" && position[8] == "O")
                return true;
            else if (position[3] == "O" && position[6] == "O" && position[9] == "O")
                return true;
            if (position[1] == "X" && position[2] == "X" && position[3] == "X")
                return true;
            else if (position[4] == "X" && position[5] == "X" && position[6] == "X")
                return true;
            else if (position[7] == "X" && position[8] == "X" && position[9] == "X")
                return true;
            else if (position[1] == "X" && position[5] == "X" && position[9] == "X")
                return true;
            else if (position[7] == "X" && position[5] == "X" && position[3] == "X")
                return true;
            else if (position[1] == "X" && position[4] == "X" && position[7] == "X")
                return true;
            else if (position[2] == "X" && position[5] == "X" && position[8] == "X")
                return true;
            else if (position[3] == "X" && position[6] == "X" && position[9] == "X")
                return true;
            else
            {
                return false;
            }
        }
        #endregion

        static string[] position = new string[10] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        #region DessinPlateau
        public static void DessinPlateau()
        {
            Console.WriteLine($"   {position[1]}  |  {position[2]}  |  {position[3]}   ");
            Console.WriteLine("-------------------");
            Console.WriteLine($"   {position[4]}  |  {position[5]}  |  {position[6]}   ");
            Console.WriteLine("-------------------");
            Console.WriteLine($"   {position[7]}  |  {position[8]}  |  {position[9]}   ");
            Console.WriteLine("");
        } 
        #endregion

    }
}